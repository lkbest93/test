packer {
  required_plugins {
    amazon = {
      version = ">= 0.0.1"
      source  = "github.com/hashicorp/amazon"
    }
  }
}

source "amazon-ebs" "ubuntu" {
  ami_name      = "bieber-${local.timestamp}"
  instance_type = "t2.micro"
  region        = "ap-northeast-2"
  source_ami = "ami-0ba5cd124d7a79612"
  subnet_id = "subnet-07482b8da844dfd83"
  associate_public_ip_address = true
  ssh_interface = "public_ip"
  ssh_username = "ubuntu"
}

build {
  sources = [
    "source.amazon-ebs.ubuntu"
  ]

  provisioner "shell" {
    environment_vars = [
      "FOO=hello world",
    ]
    inline = [
      "echo Installing Redis",
      "sleep 30",
      "sudo apt-get update",
      "sudo apt-get install -y nodejs npm git", 
      "git clone https://github.com/rheey90/aws-ec2-nodejs-server-deployment-demo.git",
    ]
  }
}
locals {
  timestamp = regex_replace(timestamp(), "[- TZ:]", "") 
}