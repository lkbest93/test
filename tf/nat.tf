
resource "aws_nat_gateway" "nat" {
  allocation_id = aws_eip.nat.id
  subnet_id     = aws_subnet.public.id
}

  
resource "aws_nat_gateway" "nat_c" {
  allocation_id = aws_eip.nat_c.id 
  subnet_id     = aws_subnet.public_c.id
}