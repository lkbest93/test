


resource "aws_eip" "bastion" {
  instance = aws_instance.bastion.id
  vpc      = true
  tags = {
    Name = "bieber-bastion"
  }
}
resource "aws_eip" "jenkins" {
  instance = aws_instance.jenkins.id
  vpc      = true
  tags = {
    Name = "bieber-jenkins"
  }
}

resource "aws_eip" "nat" {
  vpc      = true
   tags = {
    Name = "bieber-nat"
  }
}
resource "aws_eip" "nat_c" {
  vpc      = true
  tags = {
    Name = "bieber-nat_c"
  }
}
resource "aws_eip" "lb" {
  vpc      = true
  tags = {
    Name = "bieber-lb"
  }
}